#Proyecto Final Seguridad Informatica #

## Installation
Colocar dentro de la carpeta htdocs en apache o una carpeta dentro del sistema operativo

## Usage
Ejecutar desde el navegador index.html

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Objetivo
Proyecto Final de Seguridad Informática: Realizar una aplicación que permita el reforzamiento de manera lúdica de los temas vistos en la materia


## Credits
* Juan Carlos Ahuacatitan García
* Alan Rodrigo Garrido Valencia
* David Altamirano
* German


## License
* `BY-NC-SA` [Attribution-NonCommercial-ShareAlike](https://github.com/idleberg/Creative-Commons-Markdown/blob/spaces/4.0/by-nc-sa.markdown) 